"""vtest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from rest_framework.authtoken import views

from django.conf.urls.static import static
from vtest import settings

from mockapi import views as mockapi_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-token-auth/', views.obtain_auth_token),  # 获取token
    path('mdeditor/', include('mdeditor.urls')),
    re_path('api-auth/', include('rest_framework.urls'),
            name='rest_framework'),
    re_path('^jiradc/', include('jiradc.urls')),
    re_path('^mock/', include('mockapi.urls')),
    re_path('^mockapi/.+', mockapi_views.mock_api),
    re_path('^todo/', include('todo.urls')),
    re_path('^interfaces/', include('interfaces.urls')),
    re_path('^blog/', include('blog.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
