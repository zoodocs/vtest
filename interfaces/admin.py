from django.contrib import admin
from interfaces.models import Menu, Department, Env, Project

# Register your models here.

class MenuAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'level', 'parent_id', 'path')
    search_fields = ('name',)
    

class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('name',)
    

class EnvAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'address')
    search_fields = ('name', 'address')

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
    search_fields = ('name', )

admin.site.register(Menu, MenuAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(Env, EnvAdmin)
admin.site.register(Project, ProjectAdmin)
