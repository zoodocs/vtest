from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, UpdateModelMixin, CreateModelMixin, DestroyModelMixin
from rest_framework.viewsets import GenericViewSet
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.settings import api_settings

from vtest.settings import REST_FRAMEWORK

from interfaces.utils.api_response import FormatResponse

import math

FRM_RES = FormatResponse()
request_param = ['page', 'pageSize', 'kw']


class BaseListModelMixin(ListModelMixin):

    def list(self, request, *args, **kwargs):
        
        for req_key in request.query_params.keys():
            if req_key not in request_param:
                return FRM_RES.bad_request(data={'message': '请求参与有误！'})

        try:
            page_num = request.query_params['page']
        except Exception as err:
            page_num = '1'

        if page_num.isdigit():
            page_num = int(page_num)
        else:
            return FRM_RES.invalid_url_param()
    
        page_size = REST_FRAMEWORK['PAGE_SIZE']
        queryset = self.filter_queryset(self.get_queryset())
        # page_count = len(queryset)
        # max_page = math.ceil(page_count / page_size)
        # print(page_num, max_page)
        # if page_num < 0 or page_num > max_page:
        #     return FRM_RES.error_page_number()
        try:
            page = self.paginate_queryset(queryset)
            if page is not None:

                serializer = self.get_serializer(page, many=True)
                page_response = self.get_paginated_response(serializer.data)
                page_count = math.ceil(page_response.data['count'] / page_size)

                return FRM_RES.ok_list_response(page_response.data)

            serializer = self.get_serializer(queryset, many=True)
            return FRM_RES.ok_response(serializer.data)
        except Exception as err:
            try:
                kw = request.query_params['kw']
                if kw:
                    return FRM_RES.null_response()
            except Exception as e:
                return FRM_RES.bad_request(data=err)


class BaseRetrieveModelMixin(RetrieveModelMixin):
    """
    Retrieve a base model instance.
    """
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return FRM_RES.ok_response(serializer.data)


class BaseUpdateModelMixin(UpdateModelMixin):
    def update(self, request, *args, **kwargs):
        try:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)

            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}

            return FRM_RES.ok_response(serializer.data)
        except Exception as err:
            return FRM_RES.bad_request(data=err)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class BaseCreateModelMixin(CreateModelMixin):

    def create(self, request, *args, **kwargs):
        try:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return FRM_RES.ok_response(serializer.data)
            # return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        except Exception as err:
            return FRM_RES.bad_request(data=err)

    def perform_create(self, serializer):
        serializer.save()

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}


class BaseDestroyModelMixin(DestroyModelMixin):

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
            return FRM_RES.ok_response()
            # return Response(status=status.HTTP_204_NO_CONTENT)
        except Exception as err:
            try:
                kw = request.query_params['kw']
                return FRM_RES.null_response()
            except Exception as err:
                return FRM_RES.base_response()

    def perform_destroy(self, instance):
        instance.delete()


class BaseModelViewSet(
    BaseListModelMixin,
    BaseRetrieveModelMixin,
    BaseUpdateModelMixin,
    BaseCreateModelMixin,
    BaseDestroyModelMixin,
    GenericViewSet):
    pass

