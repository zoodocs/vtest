from django_filters.rest_framework import DjangoFilterBackend

from interfaces.models import Project, Department, ProjectSystem, Version
from interfaces.serializers import ProjectSerializer, ProjectSystemSerializer, VersionSerializer
from interfaces.mixins.vtest_model_mixin import BaseModelViewSet
from interfaces.paginations import CommonPagination
from interfaces.auth import IsLoginAuthenticated
from interfaces.filters import ProjectFilter, ProjectSystemFilter, VersionFilter

from interfaces.utils.api_response import FormatResponse

FRM_RES = FormatResponse()


class ProjectViewSet(BaseModelViewSet):
    permission_classes = (IsLoginAuthenticated, )
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    pagination_class = CommonPagination
    filter_backends = (DjangoFilterBackend, )
    filter_class = ProjectFilter

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            pid = instance.id
            
            project_system_obj = ProjectSystem.objects.filter(project_id=int(pid)).first()
            if project_system_obj:
                return FRM_RES.not_allow_delete(msg='删除失败：该项目存在子系统，请取消关联后再删除！')
            self.perform_destroy(instance)
            return FRM_RES.ok_response()
        except Exception as err:
            try:
                kw = request.query_params['kw']
                return FRM_RES.null_response()
            except Exception as err:
                return FRM_RES.base_response()

    def perform_destroy(self, instance):
        instance.delete()


class ProjecSystemtViewSet(BaseModelViewSet):
    permission_classes = (IsLoginAuthenticated, )
    queryset = ProjectSystem.objects.all()
    serializer_class = ProjectSystemSerializer
    pagination_class = CommonPagination
    filter_backends = (DjangoFilterBackend, )
    filter_class = ProjectSystemFilter


class VersionViewSet(BaseModelViewSet):
    permission_classes = (IsLoginAuthenticated, )
    queryset = Version.objects.all()
    serializer_class = VersionSerializer
    pagination_class = CommonPagination
    filter_backends = (DjangoFilterBackend, )
    filter_class = VersionFilter
