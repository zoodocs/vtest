from django_filters.rest_framework import DjangoFilterBackend

from interfaces.models import Department, Project
from interfaces.serializers import DepartmentSerializer
from interfaces.mixins.vtest_model_mixin import BaseModelViewSet
from interfaces.paginations import CommonPagination
from interfaces.auth import IsLoginAuthenticated
from interfaces.filters import DepartmentFilter

from interfaces.utils.api_response import FormatResponse

FRM_RES = FormatResponse()


class DepartmentViewSet(BaseModelViewSet):
    permission_classes = (IsLoginAuthenticated, )
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
    pagination_class = CommonPagination
    filter_backends = (DjangoFilterBackend, )
    filter_class = DepartmentFilter

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            pid = instance.id
            
            project_obj = Project.objects.filter(department_id=int(pid)).first()
            if project_obj:
                return FRM_RES.not_allow_delete(msg='删除失败：该部门存在项目，请取消关联后再删除！')
            self.perform_destroy(instance)
            return FRM_RES.ok_response()
        except Exception as err:
            try:
                kw = request.query_params['kw']
                return FRM_RES.null_response()
            except Exception as err:
                return FRM_RES.base_response()

    def perform_destroy(self, instance):
        instance.delete()
