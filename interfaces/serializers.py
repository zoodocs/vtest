from rest_framework import serializers
from rest_framework.serializers import UniqueTogetherValidator
from rest_framework.validators import UniqueValidator
from interfaces.models import Menu, Department, Env, Project, ProjectSystem, Version


class MenuSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    name = serializers.CharField(required=True, min_length=3, max_length=64)
    level_choices = ((0, '一级菜单'), (1, '二级菜单'))
    level = serializers.ChoiceField(choices=level_choices, required=True)
    parent_id = serializers.IntegerField(required=False)
    path = serializers.CharField(required=True)

    class Meta:
        model = Menu
        fields = ['id', 'name', 'level', 'parent_id', 'path']


class EnvSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    name = serializers.CharField(
        required=True,
        min_length=3,
        max_length=64,
    )
    address = serializers.CharField(
        required=True,
        min_length=3,
        max_length=64,
    )

    class Meta:
        model = Env
        fields = ['id', 'name', 'address']


class ProjectSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    name = serializers.CharField(
        required=True,
        min_length=3,
        max_length=64,
    )
    department_id = serializers.IntegerField(required=True)
    # department_name = serializers.ReadOnlyField(required=False, read_only=True)
    department_name = serializers.SerializerMethodField(read_only=True)
    remark = serializers.CharField(required=False, max_length=128)

    class Meta:
        model = Project
        fields = ['id', 'name', 'department_id', 'department_name', 'remark']

    def get_department_name(self, obj):
        department_obj = Department.objects.filter(
            pk=obj.department_id).first()
        return department_obj.name


class DepartmentSerializer(serializers.ModelSerializer):
    # department = ProjectSerializer(many=True)
    id = serializers.IntegerField(required=False)
    name = serializers.CharField(
        required=True,
        min_length=3,
        max_length=64,
    )

    class Meta:
        model = Department
        fields = ['id', 'name']


class ProjectSystemSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    name = serializers.CharField(
        required=True,
        min_length=3,
        max_length=64,
    )
    project_id = serializers.IntegerField(required=True)
    project_name = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ProjectSystem
        fields = ['id', 'name', 'project_id', 'project_name']

    def get_project_name(self, obj):
        project_obj = Project.objects.filter(
            pk=obj.project_id).first()
        return project_obj.name


class VersionSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    project_id = serializers.IntegerField(required=True)
    project_name = serializers.SerializerMethodField(read_only=True)
    version = serializers.CharField(required=True, min_length=1, max_length=64)
    product_manager = serializers.CharField(required=True, min_length=1, max_length=64)
    project_manager = serializers.CharField(required=True, min_length=1, max_length=64)
    frontend_dev = serializers.CharField(required=True, min_length=1, max_length=64)
    backend_dev = serializers.CharField(required=True, min_length=1, max_length=64)
    operation_dev = serializers.CharField(required=True, min_length=1, max_length=64)
    tester = serializers.CharField(required=True, min_length=1, max_length=64)
    jql = serializers.CharField(required=True, min_length=1, max_length=1024)
    test_time_consuming = serializers.IntegerField(required=True)
    test_progress = serializers.IntegerField(required=True)
    uat_date = serializers.DateField(required=False)
    submit_test_date = serializers.DateField(required=False)
    st_done_date = serializers.DateField(required=False)


    class Meta:
        model = Version
        fields = ['id', 'version', 'project_id', 'project_name', 'product_manager', 'project_manager', 'frontend_dev', 'backend_dev', 'operation_dev', 'tester',
        'jql', 'test_time_consuming', 'test_progress', 'uat_date', 'submit_test_date', 'st_done_date']

    def get_project_name(self, obj):
        project_obj = Project.objects.filter(
            pk=obj.project_id).first()
        return project_obj.name
