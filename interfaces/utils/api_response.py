from rest_framework.response import Response
from rest_framework import status
from rest_framework.utils.serializer_helpers import ReturnList, ReturnDict


class FormatResponse:

    def __init__(self):
        pass

    @staticmethod
    def base_response(status_code=None, message=None, status=False, data=None, req_type=False):
        ret_data = []
        if isinstance(data, ReturnDict):
            ret_data = [data]
        if isinstance(data, ReturnList):
            ret_data = data
        ret = {
            'code': status_code if status_code else 0,
            'message': message if message else 'Fail',
            'status': status if status else False,
            'data': ret_data,
        }
        if req_type:
            ret['total_count'] = data['count']
            ret['next_page'] = data['next']
            ret['previous_page'] = data['previous']
            ret['data'] = data['results']
        return Response(ret)

    def ok_list_response(self, data=[]):
        message = '请求成功！'
        return self.base_response(status_code=status.HTTP_200_OK, message=message, status=True, data=data,
                                  req_type=True)

    def ok_response(self, data=[]):
        message = '请求成功！'
        return self.base_response(status_code=status.HTTP_200_OK, message=message, status=True, data=data)

    def null_response(self, data=[]):
        message = '请求成功！'
        return self.base_response(status_code=status.HTTP_200_OK, message=message, status=True, data=data)

    def bad_request(self, data=[]):
        message = 'Bad Request: 请求参数错误！'
        return self.base_response(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, message=message, status=False,
                                  data=data)
    
    def not_allow_delete(self, msg=None):
        return self.base_response(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, message=msg, status=False, data=None)


    def error_params_request(self, data):
        pass

    def empty_body(self, data):
        pass

    def not_found(self, data):
        pass

    def invalid_url_param(self, data=[]):
        message = 'Invalid url: 无效参数！'
        return self.base_response(status_code=status.HTTP_404_NOT_FOUND, message=message, status=False, data=data)

    def unauthorized_request(self, data):
        pass

    def server_err_request(self, data):
        pass

    def error_page_number(self, data=[]):
        message = 'Error Page: 页码错误！'
        return self.base_response(status_code=status.HTTP_404_NOT_FOUND, message=message, status=False, data=data)
