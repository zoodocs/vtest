from django.contrib import admin
from mockapi import models

# Register your models here.

class MockConfigAdmin(admin.ModelAdmin):
    list_display = ('name', 'method', 'url', 'count', 'short_content', 'update_time', 'is_init')
    search_fields = ('name', 'url', )
    ordering = ['-create_time', ]


class MockInitAdmin(admin.ModelAdmin):
    list_display = ('name', 'init')
    search_fields = ('name', 'init')
    ordering = ['-create_time', ]


admin.site.register(models.MockConfig, MockConfigAdmin)
admin.site.register(models.MockInit, MockInitAdmin)
