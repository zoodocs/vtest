from django.db import models

# Create your models here.


class MockInit(models.Model):
    name = models.CharField('名称', max_length=256)
    init = models.CharField('初始化内容', max_length=20140)
    update_time = models.DateTimeField('最近刷新时间', auto_now=True)
    create_time = models.DateTimeField('创建时间', auto_now_add=True)

    class Meta:
        db_table = 't_mockapi_init'
        verbose_name = '模客初始化数据'
        verbose_name_plural = '模客初始化数据'

    def __str__(self):
        return self.name


class MockConfig(models.Model):
    req_model_choice = ((1, 'Get'), (2, 'Post'), (3, 'Put'), (4, 'Delete'))
    name = models.CharField('名称', max_length=256)
    method = models.IntegerField('请求方式',
                                 choices=req_model_choice,
                                 default=1,
                                 blank=True)
    url = models.CharField('接口路径', max_length=1024)
    response = models.TextField('返回报文', max_length=1024000)
    count = models.IntegerField('调用次数', default=0)
    is_init = models.BooleanField('是否初始化', default=True)
    init_id = models.IntegerField('Init ID', default=0, null=True)
    update_time = models.DateTimeField('最近刷新时间', auto_now=True)
    create_time = models.DateTimeField('创建时间', auto_now_add=True)

    class Meta:
        db_table = 't_mockapi_config'
        verbose_name = '接口模客管理'
        verbose_name_plural = '接口模客管理'

    def short_content(self):
        if len(self.response) > 100:
            return '{}...'.format(self.response[0:100])
        else:
            return self.response

    def __str__(self):
        return self.name
