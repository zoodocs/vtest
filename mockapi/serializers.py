from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from mockapi import models


class MockInitSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True, min_length=1, max_length=32, validators=[UniqueValidator(queryset=models.MockInit.objects.all(), message="名称已经存在")])
    init = serializers.CharField(required=True, min_length=0, max_length=20140)

    class Meta:
        fields = ('id', 'name', 'init')
        model = models.MockInit


class MockSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True, min_length=1, max_length=32, validators=[UniqueValidator(queryset=models.MockConfig.objects.all(), message="名称已经存在")])
    method = serializers.ChoiceField([1, 2, 3, 4])
    method_name = serializers.SerializerMethodField(read_only=True)
    url = serializers.CharField(required=True, max_length=128, min_length=1, validators=[UniqueValidator(queryset=models.MockConfig.objects.all(), message="名称已经存在")])
    response = serializers.CharField(required=True,
                                     min_length=0,
                                     max_length=1024000)
    count = serializers.IntegerField(required=False)
    is_init = serializers.BooleanField(required=False)
    init_id = serializers.IntegerField(required=False)
    init_name = serializers.SerializerMethodField(read_only=True)

    class Meta:
        fields = ('id', 'name', 'method', 'method_name', 'url', 'response',
                  'count', 'is_init', 'init_id', 'init_name')
        model = models.MockConfig

    def get_method_name(self, obj):
        req_method = {
            1: 'Get',
            2: 'Post',
            3: 'Put',
            4: 'Delete',
        }
        return req_method[obj.method]

    def get_init_name(self, obj):
        init_obj = models.MockInit.objects.filter(pk=obj.init_id).first()
        return init_obj.name
