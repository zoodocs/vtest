import django_filters
from django_filters.rest_framework import FilterSet
from django import forms

from blog import models


class BlogCategoryFilter(FilterSet):
    kw = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = models.BlogCategory
        fields = ['kw']


class BlogTagsFilter(FilterSet):
    kw = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = models.BlogTags
        fields = ['kw']


class ContentFilter(FilterSet):
    kw = django_filters.CharFilter(field_name='title', lookup_expr='icontains')

    class Meta:
        model = models.Content
        fields = ['kw']
