from django_filters.rest_framework import DjangoFilterBackend

from interfaces.mixins.vtest_model_mixin import BaseModelViewSet
from interfaces.paginations import CommonPagination
from interfaces.auth import IsLoginAuthenticated, NoAuthenticated

from interfaces.utils.api_response import FormatResponse

from blog.models import BlogCategory, BlogTags, Content
from blog.serializers import BlogCategorySerializer, BlogTagsSerializer, ContentSerializer
from blog.filters import BlogCategoryFilter, BlogTagsFilter, ContentFilter
# Create your views here.

FRM_RES = FormatResponse()


class BlogCategoryViewSet(BaseModelViewSet):
    permission_classes = (NoAuthenticated, )
    pagination_class = CommonPagination
    filter_backends = (DjangoFilterBackend, )
    queryset = BlogCategory.objects.all()
    serializer_class = BlogCategorySerializer
    filter_class = BlogCategoryFilter


class BlogTagViewSet(BaseModelViewSet):
    permission_classes = (NoAuthenticated, )
    pagination_class = CommonPagination
    filter_backends = (DjangoFilterBackend, )
    queryset = BlogTags.objects.all()
    serializer_class = BlogTagsSerializer
    filter_class = BlogTagsFilter


class ContentViewSet(BaseModelViewSet):
    permission_classes = (NoAuthenticated, )
    pagination_class = CommonPagination
    filter_backends = (DjangoFilterBackend, )
    queryset = Content.objects.all()
    serializer_class = ContentSerializer
    filter_class = ContentFilter
