from django.urls import path, re_path
from rest_framework import routers

from blog import views

router = routers.DefaultRouter(trailing_slash=False)
router.register('tags', views.BlogTagViewSet)
router.register('category', views.BlogCategoryViewSet)
router.register('detail', views.ContentViewSet)

urlpatterns = []
urlpatterns += router.urls
