from django.db import models
from mdeditor.fields import MDTextField
# Create your models here.


class BaseModel(models.Model):
    create_time = models.DateTimeField('创建时间', auto_now=True)
    update_time = models.DateTimeField('更新时间', auto_now_add=True)

    class Meta:
        abstract = True


class BlogCategory(BaseModel):
    name = models.CharField('类别名称', max_length=16, blank=True, unique=True)

    class Meta:
        db_table = 't_blog_category'
        verbose_name = '博客类别'
        verbose_name_plural = '博客类别'

    def __str__(self):
        return self.name


class BlogTags(BaseModel):
    name = models.CharField('标签名称', max_length=16, blank=True, unique=True)

    class Meta:
        db_table = 't_blog_tags'
        verbose_name = '博客标签'
        verbose_name_plural = '博客标签'

    def __str__(self):
        return self.name


class Content(BaseModel):
    title = models.CharField('标题', max_length=128, blank=True)
    # content = models.TextField('内容', blank=True, null=True)
    # content = RichTextField()
    content = MDTextField()
    type_choices = (('原创', '原创'), ('转载', '转载'))
    type = models.CharField('是否原创',
                            choices=type_choices,
                            default='原创',
                            max_length=4)
    url = models.URLField('转载URL', null=True, blank=True)
    category = models.ForeignKey(
        to=BlogCategory,
        on_delete=models.CASCADE,
    )
    # tags = models.ForeignKey(to=BlogTags, on_delete=models.CASCADE)
    tags = models.ManyToManyField(
        to=BlogTags,
        related_name='Tags',
    )

    class Meta:
        db_table = 't_blog_content'
        verbose_name = '博客管理'
        verbose_name_plural = '博客管理'

    def __str__(self):
        return self.title
