from django.apps import AppConfig


class JiradcConfig(AppConfig):
    name = 'jiradc'
