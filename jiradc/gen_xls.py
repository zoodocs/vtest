import xlwt
import json
import io


def xls(bugs_json, filename):
    bugs_list = json.loads(bugs_json)['bugs_list']
    wk = xlwt.Workbook(encoding='utf-8')
    bug_sheet = wk.add_sheet('bugslist')
    # 第一行
    # 设置背景颜色
    pattern = xlwt.Pattern()
    pattern.pattern = xlwt.Pattern.SOLID_PATTERN # May be: NO_PATTERN, SOLID_PATTERN, or 0x00 through 0x12
    pattern.pattern_fore_colour = 5 # May be: 8 through 63. 0 = Black, 1 = White, 2 = Red, 3 = Green, 4 = Blue, 5 = Yellow, 6 = Magenta, 7 = Cyan, 16 = Maroon, 17 = Dark Green, 18 = Dark Blue, 19 = Dark Yellow , almost brown), 20 = Dark Magenta, 21 = Teal, 22 = Light Gray, 23 = Dark Gray, the list goes on...
    # 设置边框
    borders = xlwt.Borders()
    borders.left = 1
    borders.right = 1
    borders.top = 1
    borders.bottom = 1
    borders.left_colour = 8
    borders.right_colour = 8
    borders.top_colour = 8
    borders.bottom_colour = 8
    # 设置字体
    font = xlwt.Font()
    font.name = '宋体'
    font.height = 260
    font.bold = True
    # 设置单元格对齐方式
    alignment = xlwt.Alignment()
    # 0x01(左端对齐)、0x02(水平方向上居中对齐)、0x03(右端对齐)
    alignment.horz = 0x02
    # 0x00(上端对齐)、 0x01(垂直方向上居中对齐)、0x02(底端对齐)
    alignment.vert = 0x01
    # 1-自动换行,0-不自动换行
    alignment.wrap = 1
    # 缩小字体填充
    alignment.shri = 0

    style = xlwt.XFStyle()  # Create the Pattern
    style.borders = borders
    style.font = font
    style.alignment = alignment
    style.pattern = pattern  # Add Pattern to Style


    alignmentx = xlwt.Alignment()
    # 0x01(左端对齐)、0x02(水平方向上居中对齐)、0x03(右端对齐)
    alignmentx.horz = 0x02
    # 0x00(上端对齐)、 0x01(垂直方向上居中对齐)、0x02(底端对齐)
    alignmentx.vert = 0x01
    # 1-自动换行,0-不自动换行
    alignmentx.wrap = 1
    # 缩小字体填充
    alignmentx.shri = 0
    stylex = xlwt.XFStyle()  # Create the Pattern
    stylex.alignment = alignmentx
    stylex.borders = borders

    alignmenty = xlwt.Alignment()
    # 1-自动换行,0-不自动换行
    alignmenty.wrap = 1
    styley = xlwt.XFStyle()  # Create the Pattern
    styley.alignment = alignmenty
    styley.borders = borders

    # 第一行宽度
    bug_sheet.col(0).width = 14 * 164
    bug_sheet.col(1).width = 14 * 250
    bug_sheet.col(2).width = 14 * 250 * 8
    bug_sheet.col(3).width = 14 * 250
    bug_sheet.col(4).width = 14 * 250
    bug_sheet.col(5).width = 14 * 250
    bug_sheet.col(6).width=14 * 250
    bug_sheet.col(7).width = 14 * 250

    # Bug列表行
    row_0 = ['序号', 'Jira链接', '标题', '经办人', '报告人', '优先级', '状态', '创建日期']
    for col in range(0, len(row_0)):
        bug_sheet.write(0, col, row_0[col], style)

    for row in range(0, len(bugs_list)):
        bug = bugs_list[row]
        bug_sheet.write(row + 1, 0, str(row + 1), stylex)
        bug_sheet.write(row + 1, 1, xlwt.Formula('HYPERLINK("http://jiradc.ipo.com/browse/{}";"{}")'.format(bug['bugid'], bug['bugid'])), stylex)
        bug_sheet.write(row + 1, 2, bug['summary'], styley)
        bug_sheet.write(row + 1, 3, bug['assignee'], stylex)
        bug_sheet.write(row + 1, 4, bug['reporter'], stylex)
        bug_sheet.write(row + 1, 5, bug['priority'], stylex)
        bug_sheet.write(row + 1, 6, bug['status'], stylex)
        bug_sheet.write(row + 1, 7, bug['created'], stylex)

    buf = io.BytesIO()
    wk.save(buf)

    return buf
    