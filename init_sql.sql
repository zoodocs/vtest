-- 初始化菜单
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('10001', '2020-05-26 17:24:31.569193', '2020-03-30 16:30:19.388278', '项目管理', '0', NULL, '/');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('10002', '2020-05-26 17:14:23.214725', '2020-05-12 16:28:21.079763', '报告管理', '0', NULL, '/');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('10003', '2020-06-03 10:10:09.395664', '2020-06-03 10:10:09.396164', '任务管理', '0', NULL, '/');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('10004', '2020-06-03 13:53:27.587890', '2020-06-03 13:53:27.587890', 'Mock管理', '0', NULL, '/');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('10005', '2020-04-03 10:53:23.398174', '2020-04-03 10:53:23.398174', '工具管理', '0', NULL, '/');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000101', '2020-04-03 15:23:44.095546', '2020-03-30 16:30:51.993906', '部门管理', '1', '10001', '/department');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000102', '2020-04-07 11:19:53.908084', '2020-03-30 16:31:01.124802', '环境管理', '1', '10001', '/env');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000103', '2020-04-07 11:21:53.791093', '2020-03-30 16:31:09.514720', '项目管理', '1', '10001', '/project');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000104', '2020-06-01 11:46:23.000000', '2020-06-01 11:46:23.000000', '系统管理', '1', '10001', '/projectSystem');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000105', '2020-06-19 16:45:39.961361', '2020-06-19 16:45:39.961361', '版本管理', '1', '10001', '/version');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000201', '2020-06-29 09:04:09.945836', '2020-06-29 09:02:51.405292', 'JQL语句管理', '1', '10002', '/jql');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000202', '2020-05-26 17:11:47.704118', '2020-05-12 16:29:55.910808', '测试进度生成', '1', '10002', '/progress');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000203', '2020-05-26 17:12:02.151693', '2020-05-26 17:10:23.231320', 'Jira缺陷分析', '1', '10002', '/jiradc');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000204', '2020-05-26 17:30:09.686574', '2020-05-26 17:30:09.686574', '日报生成', '1', '10002', '/daily');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000301', '2020-06-03 10:33:11.123151', '2020-06-03 10:33:11.123151', '项目任务管理', '1', '10003', '/projecttask');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000302', '2020-06-03 10:12:38.364430', '2020-06-03 10:12:38.364430', '我的任务', '1', '10003', '/mytask');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000303', '2020-06-03 10:14:21.106666', '2020-06-03 10:14:21.107167', '接口任务', '1', '10003', '/apitask');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000304', '2020-06-03 10:14:34.908482', '2020-06-03 10:14:34.908482', '性能任务', '1', '10003', '/tptask');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000401', '2020-06-09 18:19:10.938438', '2020-06-09 18:19:10.938438', 'Mock模板管理', '1', '10004', '/mockinit');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000402', '2020-06-03 13:53:57.451051', '2020-06-03 13:53:57.451051', 'Mock接口管理', '1', '10004', '/mock');
INSERT INTO `t_menu` (`
id`,
`create_time
`, `update_time`, `name`, `level`, `parent_id`, `path`) VALUES
('1000501', '2020-04-08 08:55:56.503745', '2020-04-03 10:53:43.152814', 'json格式化', '1', '10005', '/json');
