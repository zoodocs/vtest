# Generated by Django 2.2.2 on 2021-01-01 09:18

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectApi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_time', models.DateTimeField(auto_now=True, verbose_name='创建时间')),
                ('update_time', models.DateTimeField(auto_now_add=True, verbose_name='更新时间')),
                ('project_info_id', models.IntegerField(blank=True, verbose_name='项目任务管理ID')),
                ('name', models.CharField(max_length=256, null=True, verbose_name='接口名称')),
                ('tags_name', models.CharField(blank=True, default=None, max_length=256, null=True, verbose_name='套件名')),
                ('request_method', models.CharField(default='get', max_length=8, null=True, verbose_name='请求方式')),
                ('api_url', models.CharField(max_length=2014, verbose_name='请求路径')),
                ('status', models.IntegerField(choices=[(0, '未领取'), (1, '未完成'), (2, '已完成')], default=0, verbose_name='状态')),
                ('user_id', models.IntegerField(blank=True, verbose_name='用户ID')),
                ('priority', models.CharField(choices=[('H', 'High'), ('M', 'Medium'), ('L', 'Low')], default='M', max_length=1, verbose_name='优先级')),
                ('remark', models.CharField(blank=True, default=None, max_length=1024, null=True, verbose_name='备注')),
            ],
            options={
                'verbose_name': '接口管理',
                'verbose_name_plural': '接口管理',
                'db_table': 't_todo_project_api',
            },
        ),
        migrations.CreateModel(
            name='ProjectInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_time', models.DateTimeField(auto_now=True, verbose_name='创建时间')),
                ('update_time', models.DateTimeField(auto_now_add=True, verbose_name='更新时间')),
                ('project_id', models.IntegerField(blank=True, verbose_name='项目ID')),
                ('type', models.IntegerField(blank=True, default=0, verbose_name='项目类型')),
                ('swagger', models.CharField(blank=True, max_length=128, null=True, verbose_name='swagger地址')),
                ('git', models.CharField(blank=True, max_length=128, null=True, verbose_name='git地址')),
                ('status', models.IntegerField(blank=True, default=0, verbose_name='项目状态')),
                ('remark', models.CharField(blank=True, max_length=512, null=True, verbose_name='备注')),
                ('progress', models.IntegerField(blank=True, default=0, verbose_name='项目进度')),
                ('total_count', models.IntegerField(blank=True, default=0, null=True, verbose_name='总数')),
                ('wait_api_count', models.IntegerField(blank=True, default=0, null=True, verbose_name='待领取数')),
                ('doing_api_count', models.IntegerField(blank=True, default=0, null=True, verbose_name='待完成数')),
                ('done_api_count', models.IntegerField(blank=True, default=0, null=True, verbose_name='已完成数')),
                ('join_count', models.IntegerField(blank=True, default=0, null=True, verbose_name='参与人数')),
                ('join_user', models.CharField(blank=True, default=None, max_length=1024, null=True, verbose_name='参与人')),
            ],
            options={
                'verbose_name': '项目任务管理',
                'verbose_name_plural': '项目任务管理',
                'db_table': 't_todo_project_info',
            },
        ),
    ]
