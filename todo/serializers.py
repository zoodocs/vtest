from rest_framework import serializers
from rest_framework.serializers import UniqueTogetherValidator
from rest_framework.validators import UniqueValidator
from interfaces.models import Project
from todo.models import ProjectInfo, ProjectApi


class ProjectInfoSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    project_id = serializers.IntegerField(required=True)
    project_name = serializers.SerializerMethodField(read_only=True)
    type = serializers.IntegerField(required=True, min=0, max=2)
    swagger = serializers.CharField(required=False, max=128)
    git = serializers.CharField(required=False, max=128)
    status = serializers.IntegerField(required=False, min=0, max=4)
    remark = serializers.CharField(required=False, max=512)
    progress = serializers.IntegerField(required=False, min=0, max=100)
    total_count = serializers.IntegerField(required=False, min=0, max=10000)
    wait_api_count = serializers.IntegerField(required=False, min=0, max=10000)
    doing_api_count = serializers.IntegerField(required=False, min=0, max=10000)
    done_api_count = serializers.IntegerField(required=False, min=0, max=10000)
    join_count = serializers.IntegerField(required=False, min=0, max=10000)
    join_user = serializers.CharField(required=False, max=512)


    class Meta:
        model = ProjectInfo
        fields = [
            'id', 'project_id', 'project_name', 'type', 'swagger', 'git', 'status', 'remark', 'progress',
            'total_count','wait_api_count','doing_api_count','done_api_count','join_count','join_user'
            ]

    def get_project_name(self, obj):
        project_obj = Project.objects.filter(
            pk=obj.project_id).first()
        return project_obj.name