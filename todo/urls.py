
from django.urls import re_path, path, include
from todo import views
from rest_framework import routers

app_name = 'todo'

# router = routers.DefaultRouter(trailing_slash=False)
# router.register('mock', views.MockViewSet)

urlpatterns = [
    path('', views.todos),
]

# urlpatterns += router.urls
